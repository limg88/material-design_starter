# project

> material-design_starter

## Technologies

- [**Gulp**](http://gulpjs.com)
- [**Html**](https://developer.mozilla.org/es/docs/HTML/HTML5) 
- [**Jquery**](https://jquery.com)
- [**Bootstrap 4**](https://getbootstrap.com)
- [**Material Design**](https://mdbootstrap.com)

## Features

- Scss Compiler
- Css Minifier
- Javascript Minifier
- Javascript Concat
- Javascript Beautifier
- Images Minifier
- HTML Minifier
- Bootstrap Material Design Components
- and more...

## Install and Use

### Install

```bash
npm install
```

### Use 

```bash
gulp
```

# Author 

limg - giovanni.limatola@gmail.com

# License 

The code is available under the **limg** license. 
